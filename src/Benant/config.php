<?php
if(! defined ('__DB_INFO__')) {
	$db_info = array(
		'master' => array(
			'host' => 'localhost',
			'username' => 'userid',
			'password' => 'userpw',
			'charset' => 'utf8',
			'database' => 'database'
		),
		'select' => array(
			array(
				'host' => 'localhost',
				'username' => 'userid',
				'password' => 'userpw',
				'charset' => 'utf8',
				'database' => 'database'
			)
		)
	);
    define('__DB_INFO__', $db_info);
}
if(! defined ('__DB_INFO__')) {
	$memcache_info = array(
		'host' => '',// 'uniteglobal.pevh3i.cfg.apn2.cache.amazonaws.com',
		'port' => '11211',
	);
	define('__MEMCACHE_INFO__', $memcache_info);
}