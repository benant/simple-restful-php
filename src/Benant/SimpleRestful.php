<?php
include dirname(__file__) . '/config.php';
if (!defined('__LOADED_SIMPLERESTFUL__')) {
    /**
     * Simple Restful
     *
     * PHP, MySQL 환경에서 사용할 수 있는 간단한 Restful 클래스입니다.
     * Memcached 캐시, Memcached 세션. DB(Master/select)
     */
    class SimpleRestful
    {

        /**
         * time zone
         */
        public $time_zone= 'Asia/Seoul';

        /**
         * session directory
         */
        private $_session_dir = __DIR__.'/sessions';

        /**
         * session time
         */
        private $_session_time = 3600; // 1 hr.

        /**
         * session key name
         */
        private $_session_key_name = 'token'; // set PHPSESSIONID or custom name

        /**
         * DB 접속 정보
         */
        private $db_info = array();

        /**
         * DB 링크
         */
        private $_db_link = null;

        /**
         * DB 링크 - Master
         */
        private $_db_link_master = null;

        /**
         * DB 링크 - Seleft
         */
        private $_db_link_select = null;

        /**
         * 마지막 쿼리 실행 정보
         */
        public $_recently_query = array();

        /**
         * Simple Restful Class 생성자
         *
         * DB 접속
         * 인증 환경 설정
         * parameter 초기화 (HTML 삭제)
         * cache 폴더 지정
         * log 폴더 지정
         */
        public function __construct()
        {
            ob_start();
            error_reporting(E_ERROR | E_WARNING | E_PARSE); // 개발시 오류 메시지표시용.
            ini_set('display_error','on'); // 개발시 오류 메시지표시용.
            // ini_set('display_error','off'); // 라이브 서비스용.
            date_default_timezone_set($this->time_zone);
            $this->parameter_clean();
            $this->_set_db_info();
            $this->_db_connect();
            $this->_set_cache();
            $this->_set_session();
            $this->_set_i18n();
            $this->set_log_dir(__DIR__ . "/log/");
            // prepare tr. id
            if($_REQUEST['id'] && SimpleRestful::isTrid($_REQUEST['id'])) {
                $this->set_tr_id($_REQUEST['id']);
            }
            if($this->get_tr_id()=='') {
                $this->gen_tr_id();
            }
            register_shutdown_function(array($this, 'shutdown_function'));
        }

        /**
         * 종료시 사용할 작업.
         *
         * DB 연결 종료
         */
        public function shutdown_function()
        {
            $this->_db_close();
        }

        // --------------------------------------------------------------------- //
        // i18n

        private $i18n_lang = "ko"; // default language
        private $i18n_folder = __DIR__."/i18n";
        private $i18n_domain = "API";
        private $i18n_data = array();

        public function get_i18n_lang() {
            return $this->i18n_lang;
        }

        private function _set_i18n() {
            $lang = $this->i18n_lang;
            if(isset($_REQUEST['lang']) && trim($_REQUEST['lang'])!='') {
                $lang = $_REQUEST['lang'];
            } else if(isset($_COOKIE['lang']) && trim($_COOKIE['lang'])!='') {
                $lang = $_COOKIE['lang'];
            } else if(isset($_SESSION['lang']) && trim($_SESSION['lang'])!='') {
                $lang = $_SESSION['lang'];
            }
            $this->i18n_lang = $lang;

            $pofile = $this->i18n_folder."/{$lang}/LC_MESSAGES/".$this->i18n_domain.".po";
            $c = $this->get_cache($pofile);
            $c = $c ? json_decode($c) : false;
            if(file_exists($pofile)) {
                if($c->gentime >= filemtime($pofile)) { // 캐시 생성 시간이 파일 수정시간보다 크면 캐시 사용.
                    $this->i18n_data = (array) $c->data;
                } else {
                    $con = file_get_contents($pofile);
                    $con = preg_replace('/^\#(.*)/', '', $con); // remove comment
                    $con = preg_replace('/^\"(Project-Id-Version|Report-Msgid-Bugs-To|POT-Creation-Date|PO-Revision-Date|Last-Translator|Language-Team|Language|MIME-Version|Content-Type|Content-Transfer-Encoding|X-Generator|Plural-Forms):(.*)\"$/m', '', $con); // remove header
                    $con = str_replace('"'."\n".'"', '', $con); // concat multiline string
                    $con = explode("\n", $con);
                    $msgid = array(); $msgstr = array();
                    foreach($con as $row) {
                        if(trim($row)!='' && (strpos($row, 'msgid')!==false || strpos($row, 'msgstr')!==false)) { // 빈줄 재외, msgid, msgstr 없는것 제외
                            preg_match('/^(.*)\s"(.*)"$/', $row, $matches); // 키 "값" 으로 추출
                            if(trim($matches[1])!='') {
                                $key = $matches[1];
                                $val = $matches[2] ? $matches[2] : '';
                                if($key=='msgid' || $key=='msgstr') {
                                    array_push($$key, $val);
                                }
                            }
                        }
                    }
                    if(count($msgid)==count($msgstr)) {
                        $data = array(
                            'gentime' => time(),
                            'data' =>array()
                        );
                        for($i; $i<count($msgid); $i++) {
                            $data['data'][$msgid[$i]] = $msgstr[$i];
                        }
                        $c = $this->set_cache($pofile, json_encode($data), 31536000); // 1년 캐시
                    }
                    $this->i18n_data = (array) $data['data'];
                }
            } else {
                $this->i18n_data = array();
            }
        }

        public function __( $s ) {
            return $this->i18n_data[$s] ? $this->i18n_data[$s] : $s;
        }

        public function _e( $s ) {
            echo $this->__( $s );
        }

        // --------------------------------------------------------------------- //
        // session - memcache

        private function _set_session() {
            if(session_id()) {return true;}
            session_name($this->_session_key_name);
            if($this->cache_method == 'memcached' && is_object($this->cache_obj)) {
                session_set_save_handler(
                    array($this, "_session_open"),
                    array($this, "_session_close"),
                    array($this, "_session_read"),
                    array($this, "_session_write"),
                    array($this, "_session_destroy"),
                    array($this, "_session_gc")
                );
            } else {
                if(!file_exists($this->_session_dir)) {
                    if (!mkdir($this->_session_dir, 0777, true)) {
                        $this->error('005', str_replace('{dir}', $this->_session_dir, __('Failed to create folders. path: {dir}')));
                    }
                }
            }
            ini_set('session.cookie_domain', $domain);
            ini_set("session.cookie_lifetime", $this->_session_time);
            ini_set("session.cache_expire", $this->_session_time);
            ini_set("session.gc_maxlifetime", $this->_session_time);
            session_cache_limiter('private');
            if($_REQUEST[$this->_session_key_name]=='' && trim($_COOKIE[$this->_session_key_name])!='') {
                $_REQUEST[$this->_session_key_name] = trim($_COOKIE[$this->_session_key_name]);
            }
            $_token = trim(strip_tags($_REQUEST[$this->_session_key_name]));
            if($_token) {
                $this->token = $_token;
                session_id($_token);
                session_start();
            }
        }

        /**
         * Session Open
         */
        public function _session_open(){
            if($this->cache_method == 'memcached' && is_object($this->cache_obj)){
                return true;
            }
            return false;
        }

        /**
        * Session Close
        */
        public function _session_close(){
            $this->cache_obj->quit();
            return true;
        }

        /**
         * session id 생성
         * memcached를 공통으로 사용할 경우 중복키가 발생할 수 있어서 키에 호스트이름과 세션 구분자를 넣어 중복을 피합니다.
         */
        private function _gen_session_id($id){
            return sha1(ini_get('session.cookie_domain').'/session/'.$id);
        }

        /**
         * Session Read
         */
        public function _session_read($id){
            $id = !$id ? $this->token : $id;
            $r = $this->cache_obj->get($this->_gen_session_id($id));
            $r = $r ? unserialize(gzuncompress($r)) : '';
            if(!$r) { // NULL 일경우 write할때 오류발생하며 튕기는 현상있음. 그래서 값이 없는건 아예 지워 버림. 그러면 잘 됨.
                    $this->_session_destroy($id);
            }
            return $r;
        }

        /**
        * Session Write
        */
        public function _session_write($id, $data){
            $v = gzcompress(serialize($data));
            $r = $this->cache_obj->set($this->_gen_session_id($id), $v, time() + $this->_session_time);
            return $r;
        }

        /**
        * Session Destroy
        */
        public function _session_destroy($id){
            return $this->cache_obj->delete($this->_gen_session_id($id));
        }

        /**
         * Session Garbage Collection
         */
        public function _session_gc($sec=60*60*1){
            if($this->cache_method == 'memcached'){
                return true;
            } else {
                $this->clear_old_file($this->session_dir, $sec);
            }
        }

        public function delete_session ($sec=60*60*1) {
            $this->_session_gc($sec);
        }


        // --------------------------------------------------------------------- //
        // file

        public function check_dir($dir)
        {
            if(!file_exists($dir)) {
                if (!mkdir($dir, 0777, true)) {
                    $this->error('005', strtr(__('Failed to create folders. path: {dir}'), '{dir}', $dir));
                }
            }
            if (!is_writable($dir)) {
                $this->error('005', strtr(__('The folder does not have write permission. Please specify write permission. path: {dir}'), '{dir}', $dir));
            }
        }

        public function clear_old_file($dir, $sec = 60)
        {
            $dir = realpath($dir);
            $webdir = dirname(dirname(dirname(__DIR__)));
            if( file_exists($dir) && strpos($dir, $webdir)!==false ) {
                foreach (glob($dir . "/*") as $file) {
                    if(is_dir($file)){
                        $this->clear_old_file($file, $sec);
                        continue;
                    }
                    $t = filemtime($file);
                    $r = @ unserialize(gzuncompress(file_get_contents($file)));
                    if( $r['time'] ) {
                        $t = $r['time'];
                    }
                    if( $t < time()-$sec ) {
                        @unlink($file);
                    }
                }
            }
        }


        // --------------------------------------------------------------------- //
        // db

        private function _set_db_info() {
            $this->db_info = __DB_INFO__; // dbinfo 는 config.php에서 설정.
        }

        /**
         * DB 연결
         */
        private function _db_connect($target = '')
        {
            if (is_object($this->_db_link) && $target!='master') {
                return true;
            }
            if (is_object($this->_db_link_master) && $target=='master') {
                return true;
            }
            $db_info = array();
            // select master and select db info
            $master = $this->db_info['master'];
            $select = $this->db_info['select'][ array_rand($this->db_info['select']) ];
            if($target=='master') {
                $db_info = $master;
            } else {
                $db_info = $select;
            }
            $_db_link = mysqli_connect($db_info['host'], $db_info['username'], $db_info['password'], $db_info['database']);
            // select connect
            if (! $_db_link) {
                $this->error("001", __("Mysql Connection Error: Failed connecting to database server") . "\r\n\r\n".mysqli_connect_error()."");
            }
            if($target=='master') {
                $this->_db_link_master = $_db_link;
            } else {
                $this->_db_link_select = $_db_link;
            }
            $this->_db_link = $_db_link;
            if (!empty($db_info['charset'])) {
                $this->query('SET NAMES "' . $db_info['charset'] . '"', $target);
                mysqli_set_charset($_db_link, $db_info['charset']);
            }
            // master 연결은 필요할때 쿼리 실행전에 연결하도록 함.(select만 있는경우 불필요하게 2개씩 연결할 필요 없음)
            // Master와 Select가 같은 서버인경우 슬래이브 연결을 그대로 사용하기.
            if($target=='' && $master['host'] == $select['host']) {
                $this->_db_link_master = $this->_db_link_select;
            }
        }

        /**
         * DB 연결 종료
         */
        private function _db_close()
        {
            if ($this->_db_link) {
				if (mysqli_close($this->_db_link) ) {
					$this->_db_link = null;
				}
			}
			return true;
        }

        /**
         * DB transaction start
         */
        public function transaction_start() {
            $this->set_db_link('master');
            $this->query("SET AUTOCOMMIT=0");
            mysqli_begin_transaction($this->_db_link, MYSQLI_TRANS_START_READ_WRITE);
        }

        public function transaction_end($action='commit') {
            $this->set_db_link('master');
            if($action=='commit') {
                mysqli_commit($this->_db_link);
            } else {
                mysqli_rollback($this->_db_link);
            }
        }

        /**
         * db query function
         */
        public function query($query, $target='')
        {
            if(! $this->_db_link) {
                $this->_db_connect();
            }
            $sql_type = preg_match("/(insert |delete |update |replace |alter |create )/i", trim($query)) ? 'master' : 'select';
            $_db_link = $this->_db_link;
            if ($sql_type == 'master' && $target=='' || $target=='master') {
                if(! $this->_db_link_master) {
                    $this->_db_connect('master');
                }
                $_db_link = $this->_db_link_master;
            }
            $result = mysqli_query($_db_link, $query);
            if (!$result) {
                $this->error("005", __("Mysql Query Error: Failed executing database query")."<br />\r\nDate/Time: " . date('Y-m-d H:i:s') . "<br />\r\nQuery: $query<br />\r\nMySQL Error: " . mysqli_error($_db_link) . "");
            }
            if ($sql_type == 'master') {
                $this->_recently_query['rows_affected'] = mysqli_affected_rows($_db_link);
                if (preg_match("/^\\s*(insert|replace) /i", $query)) {
                    $this->_recently_query['last_insert_id'] = mysqli_insert_id($_db_link);
                }
            } else {
                $this->_recently_query['num_fields'] = @mysqli_num_fields($result);
                $this->_recently_query['num_rows'] = @mysqli_num_rows($result);
            }
            return $result;
        }

        /**
         * 사용할 DB를 선택할때 사용.
         * select를 지정해도 insert/update 쿼리는 마스터로 전송됩니다.
         * 가급적 insert가 있는 로직에서는 master를 선택하세요.
         */
        public function set_db_link($to = 'select') {
            $to = $to=='master' ? 'master' : 'select';
            $link_name = 'db_link_'.$to;
            if(!is_object($this->$link_name)) {
                $this->_db_connect($to);
            }
            $this->db_link = $this->$link_name;
        }

        /**
         * 쿼리 결과를 object로 받기.
         */
        public function _fetch_object($result)
        {
            return mysqli_fetch_object($result);
        }

        /**
         * 커리 결과를 array로 받기.
         */
        public function _fetch_array($result)
        {
            return mysqli_fetch_array($result);
        }

        /**
         * 쿼리를 실행하고 결과를 Object로 리턴. 단일 row 의 결과를 받을때 사용합니다.
         */
        public function query_fetch_object($query, $target='')
        {
            $result = $this->query($query, $target);
            $return = $this->_fetch_object($result);
            $this->_db_free_result($result);
            return $return;
        }

        /**
         * 쿼리를 실행하고 결과를 Array로 리턴. 단일 row 의 결과를 받을때 사용합니다.
         */
        public function query_fetch_array($query, $target='')
        {
            $result = $this->query($query, $target);
            $return = $this->_fetch_array($result);
            $this->_db_free_result($result);
            return $return;
        }

        /**
         * 쿼리를 실행하고 결과를 배열 속 Object로 리턴. 여러 row의 결과를 배열로 받을때 사용합니다.
         */
        public function query_list_object($query, $target='')
        {
            $return = array();
            $result = $this->query($query, $target);
            while ($row = $this->_fetch_object($result)) {
                if (!empty($row)) {
                    $return[] = $row;
                }
            }
            $this->_db_free_result($result);
            return $return;
        }

        /**
         * 쿼리를 실행하고 결과를 배열 속 Object로 리턴. 여러 row의 결과를 배열로 받을때 사용합니다.
         */
        public function query_list_one($query, $column_cnt=0, $target='')
        {
            $return = array();
            $result = $this->query($query, $target);
            while ($row = $this->_fetch_array($result)) {
                if (!empty($row)) {
                    $return[] = $row[$column_cnt];
                }
            }
            $this->_db_free_result($result);
            return $return;
        }

        /**
         * 쿼리를 실행하고 얻은 결과에서 첫번째 컬럼의 첫번째 row의 결과만 리턴. 단일 값을 바로 받을때 사용합니다.
         */
        public function query_one($query, $target='')
        {
            $return = array();
            $result = $this->query_fetch_array($query, $target);
            $return = $result[0] ? $result[0] : '';
            return $return;
        }

        /**
         * DB 쿼리 결과 초기화
         */
        public function _db_free_result($result)
        {
            if ($result) {mysqli_free_result($result);}
        }

        /**
         * 이스케이프 문자열로 변경. 쿼리에 사용되는 값에 적용하여 SQL Injection에 대비합니다.
         */
        public function escape($v)
        {
            return mysqli_real_escape_string($this->_db_link, $v);
        }

        /**
         * WHERE 절 생성.
         * @param Array WHERE 절에 들어갈 조건
         * 예) 키,값 형식으로 지정. 조건은 AND 로 연결. array('member_no'=>'33', 'member_no'=>'34')  -  1 and member_no='33' and member_no='33'
         * 예) 연결조건까지 지정할때 array(array('op'=>'and', 'col'=>'member_no', 'val'=>'33'), array('op'=>'or', 'col'=>'member_no', 'val'=>'34'))  -  1 and member_no='33' or member_no='33'
         * 예) 혼합조건 array('member_no'=>'33', array('op'=>'or', 'col'=>'member_no', 'val'=>'34'))  -  1 and member_no='33' or member_no='33'
         * 예) 혼합조건 array( 'txn_type'=>array('R','D'), 'status'=>array('op'=>'OR', 'val'=>'O'), 'symbol'=>$symbol, 'address_relative'=>$sender, 'amount'=>$amount );  -  1  AND txn_type IN ("R","D")   OR status="O"  AND symbol=""  AND address_relative=""  AND amount=""
         * @return String WHERE 절
         */
        private function db_gen_sql_where($where = array())
        {
            $sql_where = '1 ';
            if (is_array($where)) {
                foreach ($where as $key => $row) {
                    if(isset($row['op']) && trim($row['op'])!='') {
                        $sql_where .= ' ' . $row['op'] . ' ' . (trim($row['col'])!='' ? $row['col'] : $key) ; // '';
                        $sql_where .= is_array($row['val']) ? ' IN ("'.implode('","', array_map(array($this, 'escape'), $row['val'])).'")  ' : '="' . $this->escape($row['val']) . '" ' ;
                    } else {
                        $sql_where .= ' AND ' . $key ;
                        $sql_where .= is_array($row) ? ' IN ("'.implode('","', array_map(array($this, 'escape'), $row)).'")  ' : '="' . $this->escape($row) . '" ' ;
                    }
                }
            }
            return $sql_where;
        }

        /**
         *
         */
        private function db_gen_sql_set($data = array())
        {
            $sql_data = '';
            if (is_array($data)) {
                foreach ($data as $col => $val) {
                    if(trim($col)!='') {
                        if ($sql_data != '') {$sql_data .= ', ';}
                        $sql_data .=  $col . '="' . $this->escape($val) . '"';
                    }
                }
            }
            return $sql_data;
        }

        public function db_get_list($table, $where = array(), $rows = 10, $offset = 0)
        {
            $where = $this->db_gen_sql_where($where);
            return $this->query_list_object("SELECT * FROM {$table} WHERE {$where} LIMIT {$rows}, {$offset}");
        }

        public function db_get_row($table, $where = array())
        {
            $where = $this->db_gen_sql_where($where);
            return $this->query_fetch_object("SELECT * FROM {$table} WHERE {$where} LIMIT 1");
        }

        public function db_insert($table, $data)
        {
            $data = $this->db_gen_sql_set($data);
            return $this->query("INSERT INTO {$table} SET {$data}");
        }

        public function db_update($table, $data, $where)
        {
            $data = $this->db_gen_sql_set($data);
            $where = $this->db_gen_sql_where($where);
            return $this->query("UPDATE {$table} SET {$data} WHERE {$where}");
        }

        public function db_delete($table, $where)
        {
            $where = $this->db_gen_sql_where($where);
            return $this->query("DELETE FROM {$table} WHERE {$where}");
        }

        // --------------------------------------------------------------------- //
        // cache

        private $cache_method = 'file';
        private $cache_dir = __DIR__ . '/cache/';
        private $cache_obj = '';

        private function _set_cache() {
            $this->_set_memcache_info();
            $this->set_cache_dir($this->cache_dir);
        }

        private function _set_memcache_info() {
            if(class_exists('Memcached') && $this->cache_method=='file') {
                $memcache_info = array(
                    'host' => '192.168.0.200',
                    'port' => '11211',
                );
                if($memcache_info['host']) {
                    $this->cache_obj = new Memcached();
                    $this->cache_obj->addServer($memcache_info['host'], $memcache_info['port']);
                    $this->cache_method = 'memcached';
                }
            }
        }

        public function set_cache_dir($dir)
        {
            if($this->cache_method=='file') {
                $this->cache_dir = $dir;
                $this->check_dir($this->cache_dir);
            }
        }

        public function get_cache_dir()
        {
            return $this->cache_dir;
        }

        /**
         * 캐시 내용 가져오기.
         * @param String id cache id
         * @return Mixed cache contents.
         */
        public function get_cache($id)
        {
            if($this->cache_method=='memcached') {
				$id = $this->get_cache_file_path($id);
                $r = $this->cache_obj->get($id);
                $r = $r ? unserialize(gzuncompress($r)) : '';
            } else {
                $r = '';
                $_cache_file = $this->get_cache_file_path($id);
                if (file_exists($_cache_file)) {
                    $c = unserialize(gzuncompress(file_get_contents($_cache_file)));
                    if( $c['time'] > time()) {
                        $r = $c['contents'];
                    }
                }
            }
            return $r;
        }

        /**
         * 캐시 내용 저장.
         *
         * @param String cache id
         * @param Mixed cache contents. String or Array or Ojbect
         * @param Number cache time(sec.)
         * @return Mixed cache contents.
         */
        public function set_cache($id, $contents, $sec=60)
        {
            if($this->cache_method=='memcached') {
				$id = $this->get_cache_file_path($id);
                $v = gzcompress(serialize($contents));
				$this->cache_obj->set($id, $v, $sec<60*60*24*30 ? $sec : time() + $sec);
            } else {
                file_put_contents($this->get_cache_file_path($id), gzcompress(serialize(array('time'=>time()+$sec, 'contents'=>$contents))));
            }
            return $contents;
        }

        public function get_cache_file_path($id)
        {
            return $this->cache_dir . '.' . $this->get_cache_file_name($id);
        }

        public function get_cache_file_name($id)
        {
            return md5($_SERVER['HTTP_HOST'].'/'.$id); // 다중사이트용API인경우를 대비함
        }

        public function delete_cache ($sec = 60*60*24) {
            if($this->cache_method=='memcached') {

            } else {
                $this->clear_old_file($this->cache_dir, $sec);
            }
        }

        // --------------------------------------------------------------------- //
        // remote request

        private $curl = '';
        private $cookieJar = './cookies.txt';
        private $optTimeOut = 30;
        private $optFollowLocation = true;
        private $optReturnTransfer = true;
        private $proxyInfo = array('server'=>'','port'=>'','account'=>''); //

        public function setCookieJar ($file) {
            if(!file_exists($file)) {
                file_put_contents($file, '');
            }
            $this->cookieJar = $file;
        }

        public function getCookieJar () {
            return $this->cookieJar;
        }

        public function remote_get($p_url, $p_referer='', $p_response_header=false)
        {
            $this->remote_setup($p_url, '', $p_referer, $p_response_header);
            $_return = $this->remote_request();
            $this->remote_close();
            return $_return;
        }

        public function remote_post($p_url, $p_fields, $p_referer='', $p_response_header=false)
        {
            $this->remote_setup($p_url, $p_fields, $p_referer, $p_response_header);
            $_return = $this->remote_request();
            $this->remote_close();
            return $_return;
        }

        /**
         * get cookie value by remote request
         * @param url request url. for check domain
         * @param string cookie name.
         * @return string cookie value.
         */
        public function remote_get_cookie($p_url, $p_name) {
            $cookies = array();
            $cookiejar = $this->getCookieJar();
            // var_dump($cookiejar, $this->cookieJar); exit;
            if(file_exists($cookiejar)) {
                $cookies = file($cookiejar);
            }
            foreach($cookies as $cookie) {
                $cookie = explode("\t", $cookie);
                if(strpos($p_url, $cookie[0])!==false && $cookie[5]==$p_name) {
                    return trim($cookie[6]);
                }
            }
            return '';
        }

        function getUserAgent($p_i='') {
            $agents = array();
            $agents[] = 'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)';
            $agents[] = 'Opera/9.63 (Windows NT 6.0; U; ru) Presto/2.1.1';
            $agents[] = 'Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5';
            $agents[] = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; MRA 5.5 (build 02842); SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.2)';
            return $agents[(empty($p_i)?mt_rand(0,count($agents)-1): $p_i)];
        }

        function remote_setup($p_url, $p_fields='', $p_referer='', $p_response_header=false) {
            $this->curl = curl_init();
            curl_setopt($this->curl, CURLOPT_URL, $p_url);
            $header = array();
            $header[] = "Accept: text/xml,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
            $header[] = "Cache-Control: max-age=0";
            $header[] = "Connection: keep-alive";
            $header[] = "Keep-Alive: 300";
            $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
            $header[] = "Accept-Language: ko-kr,ko,en-us,en;q=0.5";
            $header[] = "Pragma: "; // browsers keep this blank.
            if (strpos($p_url, 'https:')===0) {
                curl_setopt ($this->curl, CURLOPT_SSL_VERIFYPEER, FALSE);
                curl_setopt ($this->curl, CURLOPT_SSLVERSION,TRUE);
            }
            if($p_response_header) {
                curl_setopt($this->curl, CURLOPT_HEADER, 1);
            }
            curl_setopt($this->curl, CURLOPT_USERAGENT, $this->getUserAgent());
            curl_setopt($this->curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($this->curl, CURLOPT_COOKIEJAR, $this->cookieJar);
            curl_setopt($this->curl, CURLOPT_COOKIEFILE, $this->cookieJar);
            curl_setopt($this->curl, CURLOPT_AUTOREFERER, FALSE);
            curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, $this->optFollowLocation);
            curl_setopt($this->curl, CURLOPT_TIMEOUT, $this->optTimeOut);
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, $this->optReturnTransfer);
            if ( $this->proxyInfo ) {
                curl_setopt($this->curl, CURLOPT_PROXY, $this->proxyInfo['server']);
                curl_setopt($this->curl, CURLOPT_PROXYPORT, $this->proxyInfo['port']);
                if ( isset($this->proxyInfo['account']) and ! empty($this->proxyInfo['account']) ) {
                    curl_setopt($this->curl, CURLOPT_PROXYUSERPWD, $this->proxyInfo['account']);
                }
            }
            if(!empty($p_referer)) {
                curl_setopt($this->curl, CURLOPT_REFERER, $p_referer);
            }
            if(!empty($p_fields)) {
                curl_setopt($this->curl, CURLOPT_POST, 1);
                curl_setopt($this->curl, CURLOPT_POSTFIELDS, $p_fields);
            }
        }

        function remote_request() {
            return curl_exec($this->curl);
        }

        function remote_close() {
            curl_close($this->curl);
        }

        // --------------------------------------------------------------------- //
        // log

        public $log_dir = '';
        public $log_name = '';
        public $logging = false;

        public function write_log($msg = '', $clear_old=false)
        {
            $flags = $clear_old ? null : FILE_APPEND;
            if ($this->logging && trim($msg) != '') {
                file_put_contents($this->get_file(), $this->get_log($msg), $flags);
            }
        }

        public function set_log_dir($dir)
        {
            $this->log_dir = $dir;
            $this->check_dir($this->log_dir);
        }
        public function set_log_name($log_name)
        {
            $this->log_name = $log_name;
        }

        public function set_logging($bool)
        {
            $this->logging = $bool ? true : false;
        }
        public function get_logging()
        {
            return $this->logging;
        }

        public function get_file()
        {
            return $this->log_dir . $this->log_name . '_' . date('Ymd') . '.log';
        }

        public function get_log($msg)
        {
            return '[' . date('Y-m-d H:i:s') . '] ' . basename(dirname($_SERVER['PHP_SELF'])) . ' ' . $msg . PHP_EOL;
        }

        public function delete_log ($sec = 60*60*24*30) {
            $this->clear_old_file($this->log_dir, $sec);
        }

        // --------------------------------------------------------------------- //
        // request

        function strip_tags_all($v) {
            if (is_array($v)) {
                foreach($v as $key => $val){
                    $v[$key] = $this->strip_tags_all($val);
                }
            } else {
                $v = trim(strip_tags($v));
            }
            return $v;
        }
        /**
         * POST, GET, REQUEST의 값에서 html 태그를 삭제한다.
         * @paran array or string $param_names html을 사용하는 인자 이름들. strip_tags를 하지 않는다.
         */
        public function parameter_clean($param_names = '')
        {
            if (!is_array($param_names)) {
                $param_names = array($param_names);
            }
            foreach ($_POST as $key => $val) {
                if (!in_array($key, $param_names)) {
                    $_POST[$key] = $this->strip_tags_all($val);
                }
            }
            foreach ($_GET as $key => $val) {
                if (!in_array($key, $param_names)) {
                    $_GET[$key] = $this->strip_tags_all($val);
                }
            }
            foreach ($_REQUEST as $key => $val) {
                if (!in_array($key, $param_names)) {
                    $_REQUEST[$key] = $this->strip_tags_all($val);
                }
            }
        }

        // --------------------------------------------------------------------- //
        // response

        /**
         * Whether to terminate API output.
         */
        public $stop_process = true;

        public function set_stop_process($v) {
            $this->stop_process = $v ? true: false;
            $GLOBALS['simplerestful']->stop_process = $v ? true: false;
        }
        public function get_stop_process() {
            return $this->stop_process;
        }

        /**
         * API Transaction ID
         */
        private $tr_id = '';

        public function get_tr_id() {
            return $this->tr_id;
        }
        public function set_tr_id($tr_id) {
            $this->tr_id = trim($tr_id);
        }
        public function gen_tr_id($tmpkey='') {
            $this->tr_id = sha1(session_id().'/'.$tmpkey.'/'.time().mt_rand(111111,999999));
        }

        public function error($code, $msg, $return_type = 'json')
        {
            $r = array(
                'success' => false,
                'error' => array(
                    'code' => $code,
                    'message' => $msg,
                ),
                'id' => $this->get_tr_id()
            );
            if($this->stop_process) {
                $this->response($r, $return_type, true);
            } else {
                throw new Exception(json_encode($r));
            }
        }

        public function success($payload=null, $return_type = 'json')
        {
            $r = array('success' => true,'id' => $this->get_tr_id() );
            if($payload!==null) { $r['payload'] = $payload; } // payload 전달 값이 없는 경우만 제외하고 나머지는 전달 받은 값 그대로 넘겨준다.
            $this->response($r, strtolower($return_type), $this->stop_process); // success 함수 후 계속 진행할지 종료할지 외부에서 결정 할 수 있도록 변경.
        }

        /**
         * API 결과값을 출력한다.
         * @global boolean $stop_log 로그 남길지 말지 여부.기본 남김(false값).
         * @param array $data 출력 내용
         */
        public function response($data = array(), $return_type = 'json', $stop_process = false)
        {
            // header & response contents 변경.
            switch($return_type) {
                case 'json' :
                    header('Content-Type: application/json');
                    $response = is_array($data) ? (empty($data) ? '[]' : json_encode($data)) : '[]';
                    break;
                case 'xml' :
                    header('Content-Type: application/xml');
                    if(!empty($data)){
                        $response = xml_encode($data);
                    }
                    break;
                case 'tsv' :
                    header('Content-Type: text/tsv');
                    if(!empty($data)){
                        $response = is_array($data) ? $data['payload'] : $data;
                    }
                    break;
                case 'csv' :
                    header('Content-Type: text/csv');
                    if(!empty($data)){
                        $response = is_array($data) ? $data['payload'] : $data;
                    }
                    break;
                case 'html' :
                    header('Content-Type: text/html');
                    if(!empty($data)){
                        $response = is_array($data) ? $data['payload'] : $data;
                    }
                    break;
            }

            // charset 변경.
            if (!empty($charset) && strtoupper($charset) != 'UTF-8') {
                $response = $this->change_charset($charset, $response);
            }

            // write log
            $this->write_log("RESPONSE: " . $response);

            // 출력
            ob_clean();
            if (!empty($charset) && strtoupper($charset) != 'UTF-8') {
                echo urldecode($response);
            } else {
                echo $response;
            }

            // 작업 종료
            if ($stop_process) {
                exit;
            }

        }

        public function change_charset($charset, $data)
        {
            foreach ($data as $key => $row) {
                if (is_object($row)) {
                    $row = (array) $row;
                }
                if (is_array($row)) {
                    $data[$key] = change_charset($charset, $row);
                } else {
                    $data[$key] = urlencode(iconv('UTF-8', $charset, $row));
                }
            }
            return $data;
        }

        // --------------------------------------------------------------------- //
        // Number

        /**
         * convert number to string
         * 10 * 0.000000000000000111 -> '10.000000000000000111'
         * 0.000000000000000111 -> '0.000000000000000111'
         * 111000000000000 * pow(10,18) -> "111000000000000000000000000000000"
         * 111000000000000 -> "111000000000000"
         * 4.0E-5 -> "0.00004"
         */
        public function numtostr($n) {
            $decimals = 0;
            $sign = '+';
            $s = strval($n);
            // 10승 확인.
            if(strpos($s, 'E')!==false) {
                $t = explode('E',$s);
                $number = $t[0];
                $decimals = substr($t[1],1);
                $sign = substr($t[1],0,1);
                // 소숫점 확인
                if(strpos($number, '.')!==false) {
                    $t = explode('.',$number);
                    $number = $t[0].$t[1];
                    if($sign=='+') {
                        $decimals -= strlen($t[1]);
                    } else {
                        $decimals -= strlen($t[0]);
                    }
                }
            } else {
                $number = $s;
            }
            if($sign=='+') {
                $s = $number . str_repeat('0',$decimals);
            } else {
                $s = '0.'.str_repeat('0',$decimals).$number;
            }
            // 소수점 뒷 0 제거.
            if(strpos($s, '.')!==false) {
                $t = explode('.', $s);
                $number = $t[0];
                $decimal = $t[1];
                $len = strlen($decimal);
                if(preg_match('/[1-9]/', $decimal)) {
                    for($i=1; $i<=$len; $i++) {
                        if(substr($decimal, $i*-1, 1)!=0) {
                            $decimal = substr($decimal, 0, $len-$i+1);
                            break;
                        }
                    }
                    $decimal = '.'.$decimal;
                } else {
                    $decimal = '';
                }
                $s = $number.$decimal;
            }
            return $s;
        }

        // ----------------------------------------------------------------- //
        // utils method

        function get_country_code_by_ip ($ip) {
            $r = $this->remote_get('https://ipapi.co/'.$ip.'/country/'); // return KR, US, ..
            $r = strpos($r, '<')!==false ? '' : $r;
            return $r;
        }

        // ----------------------------------------------------------------- //
        // validator method

        public $parameter_name = '';

        function loadParam($name)
        {
            $this->parameter_name = $name;
            return $_REQUEST[$name];
        }
        function displayParamName()
        {
            // exit($this->parameter_name);
            return $this->parameter_name ? '['.$this->parameter_name.'] ' : '';
        }

        function setDefault($s, $val)
        {
            if (trim($s) == '') {
                $s = $val;
            }
            return $s;
        }

        function checkEmpty($s, $name = '')
        {
            $name = trim($name);
            $name = $name=='' ? ($this->parameter_name ? $this->parameter_name : __('the required fields')) : $name;
            if (trim($s) == '') {
                $this->error('003', strtr(__("Please fill in {name}."), '{name}', $name));
            }
            return $s;
        }

        function checkZero($s)
        {
            if (trim($s)*1 == '0') {
                $this->error('004', $this->displayParamName().__("Please enter a number greater than zero."));
            }
            return $s;
        }

        function checkNumber($s)
        {
            if (preg_match('/[^\-\+0-9.]/', $s)) {
                $this->error('002', $this->displayParamName().__("Please enter the number."));
            }
            return $s;
        }

        /**
         * validate api transaction id
         * @param String api transaction id
         * @return Boolean validate result. true: valid. false: invalid
         */
        function isTrid($s)
        {
            if(preg_match('/[^0-9a-zA-Z]/', $s)) {
                return false;
            }
            if(strlen($s)!=40) {
                return false;
            }
            return true;
        }

    }
    $GLOBALS['simplerestful'] = new SimpleRestful;
    define('__LOADED_SIMPLERESTFUL__', true);

    // ----------------------------------------------------------------- //
    // util function

    function xml_encode($mixed, $domElement=null, $DOMDocument=null) {
        if (is_null($DOMDocument)) {
            $DOMDocument =new DOMDocument;
            $DOMDocument->formatOutput = true;
            xml_encode($mixed, $DOMDocument, $DOMDocument);
            return $DOMDocument->saveXML();
        }
        else {
            // To cope with embedded objects
            if (is_object($mixed)) {
              $mixed = get_object_vars($mixed);
            }
            if (is_array($mixed)) {
                foreach ($mixed as $index => $mixedElement) {
                    if (is_int($index)) {
                        if ($index === 0) {
                            $node = $domElement;
                        }
                        else {
                            $node = $DOMDocument->createElement($domElement->tagName);
                            $domElement->parentNode->appendChild($node);
                        }
                    }
                    else {
                        $plural = $DOMDocument->createElement($index);
                        $domElement->appendChild($plural);
                        $node = $plural;
                        if (!(rtrim($index, 's') === $index)) {
                            $singular = $DOMDocument->createElement(rtrim($index, 's'));
                            $plural->appendChild($singular);
                            $node = $singular;
                        }
                    }

                    xml_encode($mixedElement, $node, $DOMDocument);
                }
            }
            else {
                $mixed = is_bool($mixed) ? ($mixed ? 'true' : 'false') : $mixed;
                $domElement->appendChild($DOMDocument->createTextNode($mixed));
            }
        }
    }

    // ----------------------------------------------------------------- //
    // validator function

    function loadParam($s)
    {
        return $GLOBALS['simplerestful']->loadParam($s);
    }

    function setDefault($s, $val)
    {
        return $GLOBALS['simplerestful']->setDefault($s, $val);
    }

    function checkEmpty($s, $name='')
    {
        return $GLOBALS['simplerestful']->checkEmpty($s, $name);
    }

    function checkZero($s)
    {
        return $GLOBALS['simplerestful']->checkZero($s);
    }

    function checkNumber($s)
    {
        return $GLOBALS['simplerestful']->checkNumber($s);
    }

    /**
     * validate api transaction id
     * @param String api transaction id
     * @return Boolean validate result. true: valid. false: invalid
     */
    function isTrid($s)
    {
        return $GLOBALS['simplerestful']->isTrid($s);
    }

    function __($s)
    {
        return $GLOBALS['simplerestful']->__($s);
    }

    function _e($s)
    {
        return $GLOBALS['simplerestful']->_e($s);
    }

}
